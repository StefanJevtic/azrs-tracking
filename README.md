# azrs-tracking

Faculty individual project done within the "Software development tools" course.

All tools are tested on project [Paint](https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2023-2024/Paint), on branch `azrs`.

Results for each tool are recorded as an individual [issue](https://gitlab.com/StefanJevtic/azrs-tracking/-/boards), titled by tool name.
